function montaMenuCombo(url, dropdown) {
    $.ajax({
        url: url,
    }).done(function (data) {
        $('#result_get_combo_tipo_imovel').html(data);
    });
}


function fillDropDown(url, dropdown) {
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        // Clear drop down list
        $(dropdown).find("option").remove();
        $(data).each(function () {
            var $option = $("<option />");
            $option.attr("value", this.codigo).text(this.descricao);
            $(dropdown).append($option);
        });
    });
}

jQuery(function($) {
  "use strict";
    $('#get_combo_tipo input:radio').change(function(){
        var tipo_pesquisa = $(this).val();
        $('#hidden_get_combo_tipo').val(tipo_pesquisa);

        // console.log(tipo_pesquisa);

        if (tipo_pesquisa == 2){
            $('#mobiliado').hide();
        }else{
            $('#mobiliado').show();
        }


        montaMenuCombo('/get-combo-filter/'+$(this).val(), $('#get_combo_tipo_imovel'))
        fillDropDown('/get-combo-cidade/'+$(this).val(), $('#get_combo_cidade'))

				$('#get_combo_bairro').find("option").remove();
				var $option = $("<option />");
				$option.attr("value", 0).text('BAIRROS');
				$('#get_combo_bairro').append($option);
    });

		$('#get_combo_cidade').change(function(){
        fillDropDown('/get-combo-bairro/'+$(this).val()+'/'+$('#hidden_get_combo_tipo').val(), $('#get_combo_bairro'))
    });

    $('#btn_busca').click(function(){
        // var csrftoken = getCookie('csrftoken');

        $.ajax({
          url: 'busca/',
          type : "POST",
          dataType : 'html',
          data : $("#contact_busca").serialize(),
          // beforeSend: function(xhr, settings) {
          //   if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          //     xhr.setRequestHeader("X-CSRFToken", csrftoken);
          //   }
          // },
          success : function(result) {
              $('#property').html(result);
          },
          error: function(xhr, resp, text) {
              console.log(text);
          }
      })
    });
});
