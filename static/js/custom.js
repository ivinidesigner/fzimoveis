//  $(setup)
// function setup() {
//   $('.intro select').zelect({})
// }

// change style for select box
// $(".selectbox").selectbox();

jQuery(function($) {
  "use strict";

    $('#btn_ligamos').click(function(){
        if ($('#form_ligamos_fone').val() != ""){
            $.ajax({
                url: '/ligamos/',
                type : "POST",
                cache: false,
                dataType : 'json',
                // global: false,
                data : $("#form_ligamos").serialize(),
                success : function(result) {
                    if (result.type == 'success'){
                        alert (result.msg);
                    }
                },
                error: function(xhr, resp, text) {
                    alert (text);
                }
            })
        }
    });

    $('#btn_atendimento_email').click(function(){
      if ( $('#atendimento_email_nome').val() != "" && $('#atendimento_email_email').val() != "" && $('#atendimento_email_fone').val() != "" && $('#atendimento_email_mensagem').val() != ""){
        $.ajax({
            url: '/atendimento-email/',
            type : "POST",
            cache: false,
            dataType : 'json',
            // global: false,
            data : $("#atendimento_email").serialize(),
            success : function(result) {
                if (result.type == 'success'){
                    alert (result.msg);
                }
            },
            error: function(xhr, resp, text) {
              if (result.type == 'error'){
                  alert (result.msg);
              }
            }
        })
      }
    });

    $('#btn_contato_detalhes_imovel').click(function(){

      if ( $('#contato_detalhe_nome').val() != "" && $('#contato_detalhe_email').val() != "" && $('#contato_detalhe_tel').val() != "" && $('#contato_detalhe_msg').val() != ""){

        $.ajax({
            url: '/atendimento-email/',
            type : "POST",
            cache: false,
            dataType : 'json',
            // global: false,
            data : $("#form_contato_detalhes_imovel").serialize(),
            success : function(result) {
                if (result.type == 'success'){
                    alert (result.msg);
                }
            },
            error: function(xhr, resp, text) {
              if (result.type == 'error'){
                  alert (result.msg);
              }
            }
        })
      }
    });


});
